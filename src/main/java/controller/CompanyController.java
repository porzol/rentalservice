package controller;

import model.Car;
import service.CompanyService;
import service.CompanyServiceImpl;
import service.Services;

import java.io.FileNotFoundException;
import java.util.Map;

public class CompanyController {
    private CompanyService companyService = Services.getInstance().getCompanyService();

    public void listCars (){
        companyService.listCars();
    }

    public void addACar(Car car) {

            companyService.addACar(car);

    }

    public void findACar (long id){
        System.out.println(companyService.findACar(id));
    }

    public void rentCar(long car, long user){
        if(companyService.rentCar(car, user)){
            System.out.println("Car is now rented");
        }else{
            System.out.println("Car is not available");
        }
    }
    public void wirteMapToFile(){
        companyService.writeMapToFile();
    }
}
