package controller;

import service.Services;
import service.UserService;
import service.UserServiceImpl;

public class UserController {

    private UserService userService = Services.getInstance().getUserService();

    public void registerUser(String login, String password) {
        if (userService.registerUser(login, password)) {
            System.out.println("Udało Ci się zarejestrować, Witamy!");

        } else {
            System.err.println("Nie udało się zarejestrować :(");
        }
    }

    public Long login(String login, String pass) {
        return userService.loginUser(login, pass);
    }

}
