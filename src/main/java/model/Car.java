package model;



public class Car {
    private String make;
    private double engine;
    private int numberOfSeats;
    private long carID;
    private boolean rented = false;
    private long rentedBy;


    public long getCarID() {
        return carID;
    }

    public void setCarID(long carID) {
        this.carID = carID;
    }

    public Car(String make, double engine, int numberOfSeats, boolean rented, long carID) {
        this.make = make;
        this.engine = engine;
        this.numberOfSeats = numberOfSeats;
        this.carID = carID;
        this.rented = rented;
    }

    public boolean isRented() {
        return rented;
    }

    public void setRented(boolean rented) {
        this.rented = rented;
    }

    public long getRentedBy() {
        return rentedBy;
    }

    public void setRentedBy(long rentedBy) {
        this.rentedBy = rentedBy;
    }

    @Override
    public String toString() {
        return "ID " + carID + " Make " + make + " Engine " + engine + " Seats " + numberOfSeats + " Rented " + rented + "NEXT";
    }
//        return "ID " +
//                "make= " + make +
//                ", engine= " + engine +
//                ", numberOfSeats= " + numberOfSeats +
//                ", carID= " + carID +
//                ", rented= " + rented +
//                "\n";
//    }

    public Car() {
    }

    public static Car createBMW16(){


            return new Car ("BMW",2,5,false, 0);

        }
        public static Car createAudi(){

            return new Car ("AUDI",3,5,false, 0);
        }




}
