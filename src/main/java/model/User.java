package model;

import java.util.ArrayList;
import java.util.List;

public class User {
    private long id;
    private String name;
    private String login;
    private String passwordHash;
    private boolean registered = false;
    private boolean loggedIn = false;


    public User() {
    }

    public User(long id, String login, String passwordHash, boolean registered) {
        this.id = id;
        this.login = login;
        this.passwordHash = passwordHash;
        this.registered = false;
    }

    public User(long id, String name, String login, String passwordHash) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public boolean isRegistered() {
        return registered;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }


    }

