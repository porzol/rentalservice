package views;

import controller.CompanyController;
import controller.UserController;
import model.Car;
import model.User;
import service.CompanyService;
import service.CompanyServiceImpl;

public class CommandParser {
    private UserController userController = new UserController();
    private CompanyController companyController = new CompanyController();
    private SessionCookies cookies = new SessionCookies();


    public void parseLine(String line) {
        String[] words = line.trim().split(" ");
        if (words[0].toLowerCase().equals("user")) {
            parseUserCommand(words);
        }
        if (words[0].toLowerCase().equals("admin")) {
            companyController.addACar(Car.createAudi());
        }
        if (words[0].toLowerCase().equals("save")){
            companyController.wirteMapToFile();
        }

    }

    private void parseUserCommand(String[] words) {
        if (words[1].equals("register")) {
            userController.registerUser(words[2], words[3]);
        } else if (words[1].equalsIgnoreCase("login")) {
            if (!cookies.isLoggedIn()) {
                // próbujemy sie zalogować
                Long userId = userController.login(words[2], words[3]);
                if (userId != null) {

                    // zalogowani
                    cookies.setLoggedIn(true);
                    cookies.setUserId(userId);
                    System.out.println("logowanie zakończone powodzeniem");
                } else {
                    // nieudana próba logowania
                    cookies.setLoggedIn(false);
                    System.out.println("logowanie zakończone niepowowdzeniem");
                }
            }
        }
       else if (words[1].toLowerCase().equals("list")) {
            if (cookies.isLoggedIn()) {
                companyController.listCars();
            } else System.out.println("nie masz uprawnień");
        }
        else if (words[1].toLowerCase().equals("find")){
            long idToRent = Long.parseLong(words[2]);
            companyController.findACar(idToRent);
        }
        else if (words[1].toLowerCase().equals("rent")){
            long iddToRent = Long.parseLong(words[2]);
            companyController.rentCar(iddToRent, cookies.getUserId() );
        }

    }

}