package views;

import controller.CompanyController;
import model.Car;
import model.Company;
import service.CompanyService;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        CommandParser parser = new CommandParser();

        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.equalsIgnoreCase("quit")) {
                break;
            } else {
                parser.parseLine(line);
            }
        }

    }
}