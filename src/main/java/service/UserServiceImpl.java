package service;

import model.Car;


import model.Company;

import model.User;
import org.apache.commons.codec.digest.DigestUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class UserServiceImpl implements UserService {

    private static long USER_ID_COUNTER = 0;

    private Map<Long, User> users = new HashMap<>(); // mapa id -> user

    @Override
    public User getUserWithID(long ID) {
        return users.get(ID);
    }



    public boolean registerUser(String login, String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");

            String hashed = DigestUtils.md5Hex(password).toLowerCase();

            System.out.println("Encrypted password is: " + hashed);

            User newUser = new User(++USER_ID_COUNTER, login, hashed, true);
            System.out.println("Twój ID to: " + USER_ID_COUNTER);
            users.put(newUser.getId(), newUser);
            return true;
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Unable to encrypt password - no such algorithm.");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean userExists(User user) {
       if (user.isRegistered()) {
           return true;
       }
       return false;
    }

    @Override
    public Long loginUser(String login, String password) {
        List<User> matchingUsers = users.values().stream().filter(user-> user.getLogin().equals(login)).collect(Collectors.toList());

        if(matchingUsers.size() == 1){
            User matchingUser = matchingUsers.get(0);
            String hashed = DigestUtils.md5Hex(password).toLowerCase();

            if(matchingUser.getPasswordHash().equals(hashed)){
                return matchingUser.getId();
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
}