package service;



public class Services {
    private final static Services ourInstance = new Services();



    private final CompanyService companyService = new CompanyServiceImpl();

    private final UserService userService = new UserServiceImpl();



    public CompanyService getCompanyService() {
        return companyService;
    }



    public UserService getUserService() {
        return userService;
    }

    public static Services getInstance() {
        return ourInstance;
    }


}
