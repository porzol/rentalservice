package service;

import model.Car;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface CompanyService {
  void addACar (Car car);
  void listCars ();
  Car findACar (long id);
  boolean rentCar(long carId, long rentierId);
  void writeMapToFile();




}
