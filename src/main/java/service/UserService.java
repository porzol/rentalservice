package service;

import model.User;

public interface UserService {
    User getUserWithID (long ID);
    boolean registerUser(String login, String password);
    boolean userExists(User user);
    Long loginUser(String login, String password);

}
