package service;

import model.Car;
import model.User;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CompanyServiceImpl implements CompanyService {
    List<Car> carList = new ArrayList<>();
    File file = new File("database.txt");
    private int counter = 1;

    @Override
    public Car findACar(long id) {
        for (Car car : carList) {
            if (car.getCarID() == id) {
                return car;
            }
        }
        return null;
    }

    @Override
    public void addACar(Car car) {
        carList.add(car);
        car.setCarID(counter++);
    }

    @Override
    public void listCars() {
        if (!carList.isEmpty()) {
            for (Car auto : carList) {
                System.out.println(auto);
            }
        } else System.out.println("error");
    }

    @Override
    public boolean rentCar(long carId, long rentierId) {
        Car c = findACar(carId);
        if (c.isRented()) {
            return false;
        } else {
            c.setRentedBy(rentierId);
            c.setRented(true);
            return true;
        }
    }

    @Override
    public void writeMapToFile() {
        BufferedWriter bw = null;
        try {
            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            if (!file.exists()){
                file.createNewFile();
            }
            bw.write(carList.toString());
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}



